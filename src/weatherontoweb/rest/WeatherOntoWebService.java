package weatherontoweb.rest;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.semanticweb.HermiT.Configuration;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.RDFXMLOntologyFormat;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
 
@Path("/")
public class WeatherOntoWebService {
 
	/**
	 * Variable declarations
	 */
	private static String[] requiredAPIData = {"temp_c", "dewpoint_c", "precip_today_metric","relative_humidity","wind","solarradiation","icon"};
	
	private static String ApiURL;
	JSONObject apiJsonData;
	ArrayList<JSONObject> objProperties = new ArrayList<JSONObject>();
	ArrayList<String> objectWeatherState= new ArrayList<String>();
	HashMap<String,ArrayList> objClothes = new HashMap<String,ArrayList>();
	

	private static String NS;
	private static File ontFile; 
	private static File newOntfile;
	
	/**
	 * Default constructor
	 */
	public WeatherOntoWebService(){
		
		ApiURL="http://api.wunderground.com/api/89e41b1bcea993af/conditions/q/GA/Athens.json";
		NS = "https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/WeatherOntology.owl#";
		ontFile = new File("/Users/admin/Documents/workspace/WeatherOntoWeb/WeatherOntoWeb/src/main/webapp/ontologies/WeatherOntology.owl");
		newOntfile = new File("/Users/admin/Documents/workspace/WeatherOntoWeb/WeatherOntoWeb/src/main/webapp/ontologies/InferredWeatherOntology.owl");
		
	}
	
	@GET
	@Path("/weather")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWeatherState() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = "";
		
		try{
			
			
			/**
			 * Call to all methods
			 */
			this.getDataFromAPI();
			this.putAPIData();
			this.determineWeatherState();
			this.populateOnto();
			this.determineWeatherClothes();
			jsonOutput=gson.toJson(this.formatOutput());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonOutput;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject formatOutput() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JSONObject jsonObj=new JSONObject();
		
		try{
			
			LinkedHashMap<Object,Object> objPropertiesTemp = new LinkedHashMap<Object,Object>();
			
			for(int i=0;i<this.objProperties.size();i++){
				JSONObject currObj=this.objProperties.get(i);
				if(currObj.get("property").equals("hasWind")){
					objPropertiesTemp.put(currObj.get("class"),currObj.get("literalSpeed"));
				}else{
					objPropertiesTemp.put(currObj.get("class"),currObj.get("literal"));
				}
			}
			
			jsonObj.put("Weather States", this.objectWeatherState);
			jsonObj.put("Suitable Clothes", this.objClothes);
			jsonObj.put("Weather Conditions", objPropertiesTemp);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	public String getDataFromAPI(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput1="";
		
		try{
			URL weatherUrl = new URL(ApiURL);
			HttpURLConnection conn = (HttpURLConnection) weatherUrl.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			JSONParser parser = new JSONParser();	
			apiJsonData = (JSONObject)parser.parse(br);
			jsonOutput1 = gson.toJson(apiJsonData);
	
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return jsonOutput1;
	}
	
	/**
	 * Method to structure the data received from API data
	 * into objectProperties 
	 */
	public void putAPIData(){
		try{
			JSONObject ob1 = (JSONObject) this.apiJsonData.get("current_observation");

			for(int i =0;i<requiredAPIData.length;i++ )	{

				if(requiredAPIData[i].equalsIgnoreCase("temp_c")){
					this.calculateTemperature(Float.parseFloat(ob1.get(requiredAPIData[i]).toString()));
				}else if(requiredAPIData[i].equalsIgnoreCase("relative_humidity")){
					String humidity=ob1.get(requiredAPIData[i]).toString();
					String substringHumidity=humidity.substring(0,humidity.length()-1);
					this.calculateHumidity(Float.parseFloat(substringHumidity));

				}else if(requiredAPIData[i].equalsIgnoreCase("solarradiation")){
					if(!ob1.get(requiredAPIData[i]).toString().equals("--")){
						this.calculateSolarRadiance(Float.parseFloat(ob1.get(requiredAPIData[i]).toString()));
					}
				}else if(requiredAPIData[i].equalsIgnoreCase("precip_today_metric")){
					this.calculatePrecipitation(Float.parseFloat(ob1.get(requiredAPIData[i]).toString()));

				}else if(requiredAPIData[i].equalsIgnoreCase("dewpoint_c")){
					this.calculateDewPoint(Float.parseFloat(ob1.get(requiredAPIData[i]).toString()));
				}else if(requiredAPIData[i].toLowerCase().contains("wind")){
					this.calculateWind(Float.parseFloat(ob1.get("wind_kph").toString()),Float.parseFloat(ob1.get("wind_degrees").toString()));
				}else if(requiredAPIData[i].equalsIgnoreCase("icon")){
					this.calculateCloudCover(ob1.get("icon").toString());
				}	
			}
		}
		
		catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Generic Method used to put every JSON object in object properties	
	 * @param individualJSONObj
	 */
	@SuppressWarnings("unchecked")
	public void addJsonToObjectProperties(String className, String propertyName, String value1, Object literalValue1, String value2, Object literalValue2){
		JSONObject returnJson=new JSONObject();
		try{
			
			returnJson.put("class",className);
			returnJson.put("property",propertyName );
			
			if(propertyName.equals("hasCloudCover")) {
				returnJson.put("value",value1);
				returnJson.put("literal",literalValue1);
			}else if(propertyName.equals("hasWind")){
				returnJson.put("speedValue",value1 );
				returnJson.put("literalSpeed",literalValue1 );
				returnJson.put("dirValue",value2 );
				returnJson.put("literalDirection",literalValue2 );
				
			}else{
				returnJson.put("value",value1);
				returnJson.put("literal",literalValue1);
			}
			this.objProperties.add(returnJson);

			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 
	 * Method to determine the cloud cover
	 * @String cloudCover
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculateCloudCover(String cloudCover){
		try{
			String className="";
			String propertyName = "hasCloudCover";
			String value = "hasCloudCoverValue";
			int cloudCoverLiteral=0;
			Random r = new Random();
			
			if(cloudCover.toLowerCase().contains("mostly")){
				cloudCoverLiteral = r.nextInt(8-6) + 6;
				className = "MostlyCloudy";
				this.addJsonToObjectProperties(className, propertyName, value, cloudCoverLiteral, null, null);
			}if(cloudCover.toLowerCase().contains("partly")){
				cloudCoverLiteral = r.nextInt(5-1) + 1;
				className = "PartlyCloudy";
				this.addJsonToObjectProperties(className, propertyName, value, cloudCoverLiteral, null, null);
			}if(cloudCover.equalsIgnoreCase("cloudy")){
				cloudCoverLiteral = 8;
				className = "Overcast";
				this.addJsonToObjectProperties(className, propertyName, value, cloudCoverLiteral, null, null);
			}if(cloudCover.toLowerCase().contains("clear")){
				cloudCoverLiteral = 0;
				className = "NoCloudCover";
				this.addJsonToObjectProperties(className, propertyName, value, cloudCoverLiteral, null, null);
			}if(cloudCover.toLowerCase().contains("rain")){
				cloudCoverLiteral = 9;
				className = "UnknownCloudCover";
				this.addJsonToObjectProperties(className, propertyName, value, cloudCoverLiteral, null, null);
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
	/**
	 * Method to determine the temperature
	 * @float temperatureValue
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculateTemperature(Float temperatureValue){
		try{

			String className="";
			String propertyName = "hasExteriorTemperature";
			String value = "hasValue";
			float literalValue = temperatureValue ;

			if(temperatureValue>25.0){
				className = "AboveRoomTemperature";
				
			}if(temperatureValue>0.0){
				className = "AboveZeroTemperature";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if(temperatureValue<=0.0){
				className = "BelowOrZeroTemperature";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if(temperatureValue<20.0){
				className = "BelowRoomTemperature";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if(temperatureValue<-25.0){
				className = "ExtremeFrost";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if(temperatureValue>=37.0){
				className = "ExtremeHeat";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if((temperatureValue>-25.0) && (temperatureValue<0.0)){
				className = "Frost";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if(temperatureValue>=30.0){
				className = "Heat";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}if((temperatureValue>=20.0) && (temperatureValue<=25.0)){
				className = "RoomTemperature";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}
			
			
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	

	/**
	 * 
	 * Method to determine humidity 
	 * @float humidityValue
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculateHumidity(Float humidityValue){
		
		try{
			String className="";
			String propertyName = "hasHumidity";
			String value = "hasValue";
			float literalValue = humidityValue;
			
			if(humidityValue<=25.0){
				className = "DryHumidity";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if(humidityValue>60.0){
				className = "MoistHumidity";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((humidityValue>25.0)&&(humidityValue<=60.0)){
				className = "OptimumHumidity";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

			}	

		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	
     /**
      * Method to determine Solar Radiance
      * @float solarValue
      * @return JSONObject
      */
	@SuppressWarnings("unchecked")
	public void calculateSolarRadiance(Float solarValue){
		try{
			String className="";
			String propertyName = "hasSolarIrradiation";
			String value = "hasValue";
			float literalValue = solarValue ;
			
			if(solarValue>500.0){
				className = "HighSolarIrradiance";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((solarValue>50.0) && (solarValue<=250.0)){
				className = "LowSolarIrradiance";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((solarValue>250.0) && (solarValue <=500.0)){
				className = "ModerateSolarIrradiance";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((solarValue>=0.0) && (solarValue<=50.0)){
				className = "NeglectibleSolarIrradiance";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Method to determine the dew point
	 * @float dewValue
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculateDewPoint(Float dewValue){
		try{
			String className="DewPointTemperature";
			String propertyName = "hasDewPointTemperature";
			String value = "hasValue";
			float literalValue = dewValue ;
		
		this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);

	}catch(Exception e){
		e.printStackTrace();
	}
}
	
	
	
	/**
	 * Method to determine Precipitation
	 * @Float precipitationValue
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculatePrecipitation(Float precipitationValue){
		
		try{
			String className="";
			String propertyName = "hasPrecipitation";
			String value = "hasIntensity";
			float literalValue = precipitationValue ;
			
			if(precipitationValue>4.0){
				className = "HeavyPrecipitation"; 
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((precipitationValue>0.0) && (precipitationValue<=1.0)){
				className = "LightPrecipitation";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((precipitationValue>1.0) && (precipitationValue <=4.0)){
				className = "ModeratePrecipitation";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}else if((precipitationValue==0.0)){
				className = "NoPrecipitation";
				this.addJsonToObjectProperties(className, propertyName, value, literalValue, null, null);
			}
			
			}catch(Exception e){
			e.printStackTrace();
		}

	}

	
	
	/**
	 * 
	 * Method to determine Wind
	 * @param windValue
	 * @return JSONObject
	 */
	@SuppressWarnings("unchecked")
	public void calculateWind(Float literalSpeedValue ,Float literalDirValue){
		try{
			String className="";
			String propertyName = "hasWind";
			String speedValue = "hasSpeed";
			String dirValue = "hasDirection";
		    literalSpeedValue = literalSpeedValue * (5/18);
			if((literalSpeedValue>7.9) && (literalSpeedValue<=10.7)){
				className = "FreshBreeze";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>17.1) && (literalSpeedValue<=20.7)){
				className = "FreshGale";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>3.4) && (literalSpeedValue<=5.4)){
				className = "GentleBreeze";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>32.6)){
				className = "Hurricane";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>0.2) && (literalSpeedValue<=1.5)){
				className = "LightAir";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>1.5) && (literalSpeedValue<=3.4)){
				className = "LightBreeze";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>0.2) && (literalSpeedValue<=10.7)){
				className = "LightWind";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>5.4) && (literalSpeedValue<=7.9)){
				className = "ModerateBreeze";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>13.8) && (literalSpeedValue<=17.1)){
				className = "ModerateGale";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>=0.0) && (literalSpeedValue<=0.2)){
				className = "NoWind";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>24.4)){
				className = "Storm";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>10.7) && (literalSpeedValue<=13.8)){
				className = "StrongBreeze";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>20.7) && (literalSpeedValue<=24.4)){
				className = "StrongGale";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>10.7) && (literalSpeedValue<=24.4)){
				className = "StrongWind";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>28.4) && (literalSpeedValue<=32.6)){
				className = "ViolentStorm";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}if((literalSpeedValue>24.4) && (literalSpeedValue<=28.4)){
				className = "WholeGale";
				this.addJsonToObjectProperties(className, propertyName, speedValue, literalSpeedValue, dirValue, literalDirValue);
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

 
	/**
	 * Method to determine the Weather State
	 * 
	 */
	public void determineWeatherState(){
		try{
			
			//Retrieve only the class names
			ArrayList<String> arr= new ArrayList<String>();
			int i=0;
			for (i=0;i<this.objProperties.size();i++){
				//System.out.println(this.objProperties.get(i).get("class").toString());
				arr.add(i,this.objProperties.get(i).get("class").toString());
				
			}
						
		    if((!arr.contains("AboveRoomTemperature")) && arr.contains("NoPrecipitation") && (arr.contains("LightWind") || arr.contains("StrongWind"))){	
				//System.out.println("This is Airing WeatherState");
				this.objectWeatherState.add("AiringWeatherState");
			}
			
		    if(arr.contains("BelowRoomTemperature")){
				//System.out.println("This is Cooling WeatherState");
				this.objectWeatherState.add("CoolingWeatherState");
			}
			
			if((arr.contains("NoPrecipitation")) 
					&& ((arr.contains("LightWind")) || (arr.contains("NoWind")))
					&& ((arr.contains("NoCloudCover")) || (arr.contains("PartlyCloudy")))){
				//System.out.println("This is Fair WeatherState");
				this.objectWeatherState.add("FairWeatherState");
			}
			
			if((arr.contains("AboveRoomTemperature"))){
				
				//System.out.println("This is Heating WeatherState");
				this.objectWeatherState.add("HeatingWeatherState");
				
			}
			
			if((arr.contains("MoistHumidity"))){
				
				//System.out.println("This is Humidifying WeatherState");
				this.objectWeatherState.add("HumidifyingWeatherState");
			}
			
			if(!(arr.contains("NoPrecipitation"))
					&& (arr.contains("AboveZeroTemperature"))){
				
				//System.out.println("This is Rainy WeatherState");
				this.objectWeatherState.add("RainyWeatherState");
			}
			
			if(((arr.contains("HeavyPrecipitation"))
					&& (arr.contains("StrongWind")))
					|| (arr.contains("ExtremeFrost"))
					|| (arr.contains("ExtremeHeat"))
					|| (arr.contains("HeavyPrecipitation"))
					|| (arr.contains("Storm"))){
				
				//System.out.println("This is Severe WeatherState");
				this.objectWeatherState.add("SevereWeatherState");
				
			}
			 if(!(arr.contains("NoPrecipitation"))
						&& (arr.contains("BelowOrZeroTemperature"))
						&& (arr.contains("Precipitation"))){
				// System.out.println("This is Snowy WeatherState");
				 this.objectWeatherState.add("SnowyWeatherState");
			 }
			 if((arr.contains("HighSolarIrradiance"))
					 	&& (arr.contains("NoCloudCover"))
						&& (arr.contains("NoPrecipitation"))){
				// System.out.println("This is Sunny WeatherState");
				 this.objectWeatherState.add("SunnyWeatherState");
			 }
			 if((arr.contains("AboveZeroTemperature"))
						&& (arr.contains("HeavyPrecipitation"))
						&& (arr.contains("LightWind")) 
									|| (arr.contains("Storm")) 
									|| (arr.contains("StrongWind"))){
				// System.out.println("This is Thunderstorm WeatherState");
				 this.objectWeatherState.add("Thunderstorm");
				 
			 }

			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	/**
	 * Method to populate the ontology
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void populateOnto(){
		try{
			
			OWLOntologyManager owlOntologyManagerObject = OWLManager.createOWLOntologyManager();
			OWLOntology owlOntologyObject = owlOntologyManagerObject.loadOntologyFromOntologyDocument(ontFile);

			Configuration configuration = new Configuration();
			configuration.ignoreUnsupportedDatatypes = true;

			OWLDataFactory owlDataFactoryObject = owlOntologyManagerObject.getOWLDataFactory();
			
			
			/**
			 * Create a Weather state object to represent the current weather
			 * Also, add it to Ontology
			 */
			OWLClass weatherStateClass = owlDataFactoryObject.getOWLClass(IRI.create(NS + "WeatherState"));
			OWLNamedIndividual WeatherState_Current = owlDataFactoryObject.getOWLNamedIndividual(IRI.create(NS+ "WeatherState_Current"));
			OWLClassAssertionAxiom owlClassAssertionAxiom1 = owlDataFactoryObject.getOWLClassAssertionAxiom(weatherStateClass, WeatherState_Current);
			owlOntologyManagerObject.addAxiom(owlOntologyObject, owlClassAssertionAxiom1);
		
			
			//For each of the object property
			for(int i=0;i<this.objProperties.size();i++){
				
				JSONObject individualProperty=this.objProperties.get(i);
			
				/**
				 * 1. Determine the class and retrieve it from the Ontology
				 * 2. Create its corresponding object
				 * 3. Add the newly created object to Ontology model
				 */
				OWLClass classObject = owlDataFactoryObject.getOWLClass(IRI.create(NS + individualProperty.get("class")));
				String individualName=individualProperty.get("class")+"_Current";
				OWLNamedIndividual individualObject = owlDataFactoryObject.getOWLNamedIndividual(IRI.create(NS + individualName));
				OWLClassAssertionAxiom classAssertionAxiomObject2 = owlDataFactoryObject.getOWLClassAssertionAxiom(classObject, individualObject);
				owlOntologyManagerObject.addAxiom(owlOntologyObject, classAssertionAxiomObject2);
				
				/**
				 * 1. Determine the value property and retrieve it from Ontology. Eg: hasValue
				 * 2. Create a literal for assigning the value
				 * 3. Assign the literal value to the property and the individual. Temperature6 hasValue 30
				 */
				if((individualProperty.containsKey("value")) && (individualProperty.get("value").equals("hasCloudCoverValue"))){
				
					OWLDataProperty valueDataProperty = owlDataFactoryObject.getOWLDataProperty(IRI.create(NS+ individualProperty.get("value")));
					OWLLiteral literalObject = owlDataFactoryObject.getOWLLiteral(Integer.parseInt(individualProperty.get("literal").toString()));
					OWLDataPropertyAssertionAxiom owlDataPropertyAssertionAxiom1 = owlDataFactoryObject.getOWLDataPropertyAssertionAxiom(valueDataProperty, individualObject, literalObject);
					AddAxiom individualAxiom_3 = new AddAxiom(owlOntologyObject, owlDataPropertyAssertionAxiom1);
					owlOntologyManagerObject.applyChange(individualAxiom_3);
			
				}else if( (individualProperty.containsKey("property")) && (individualProperty.get("property").equals("hasWind"))){
					
					OWLDataProperty valueDataProperty1 = owlDataFactoryObject.getOWLDataProperty(IRI.create(NS+ individualProperty.get("speedValue")));
					OWLDataProperty valueDataProperty2 = owlDataFactoryObject.getOWLDataProperty(IRI.create(NS+ individualProperty.get("dirValue")));
					
					OWLLiteral literalObject1 = owlDataFactoryObject.getOWLLiteral(Float.parseFloat(individualProperty.get("literalSpeed").toString()));
					OWLLiteral literalObject2 = owlDataFactoryObject.getOWLLiteral(Float.parseFloat(individualProperty.get("literalDirection").toString()));
					
					OWLDataPropertyAssertionAxiom owlDataPropertyAssertionAxiom1 = owlDataFactoryObject.getOWLDataPropertyAssertionAxiom(valueDataProperty1, individualObject, literalObject1);
					OWLDataPropertyAssertionAxiom owlDataPropertyAssertionAxiom2 = owlDataFactoryObject.getOWLDataPropertyAssertionAxiom(valueDataProperty2, individualObject, literalObject2);
					
					AddAxiom individualAxiom_3 = new AddAxiom(owlOntologyObject, owlDataPropertyAssertionAxiom1);
					AddAxiom individualAxiom_4 = new AddAxiom(owlOntologyObject, owlDataPropertyAssertionAxiom2);
					
					owlOntologyManagerObject.applyChange(individualAxiom_3);
					owlOntologyManagerObject.applyChange(individualAxiom_4);
				
				}else{
					
					OWLDataProperty valueDataProperty = owlDataFactoryObject.getOWLDataProperty(IRI.create(NS+ individualProperty.get("value")));
					OWLLiteral literalObject = owlDataFactoryObject.getOWLLiteral(Float.parseFloat(individualProperty.get("literal").toString()));
					OWLDataPropertyAssertionAxiom owlDataPropertyAssertionAxiom1 = owlDataFactoryObject.getOWLDataPropertyAssertionAxiom(valueDataProperty, individualObject, literalObject);
					AddAxiom individualAxiom_5 = new AddAxiom(owlOntologyObject, owlDataPropertyAssertionAxiom1);
					owlOntologyManagerObject.applyChange(individualAxiom_5);
					
				}
				
				/**
				 * Assign that particular object property to Weather state
				 * Eg: WeatherState6 hasExteriorTemperature AboveRoomTemperature
				 */
				OWLObjectProperty objectProperty = owlDataFactoryObject.getOWLObjectProperty(IRI.create(NS + individualProperty.get("property")));
				OWLObjectPropertyAssertionAxiom owlObjectPropertyAssertionAxiom1 = owlDataFactoryObject.getOWLObjectPropertyAssertionAxiom(objectProperty, WeatherState_Current, individualObject);
				AddAxiom individualAxiom_5 = new AddAxiom(owlOntologyObject, owlObjectPropertyAssertionAxiom1);
				owlOntologyManagerObject.applyChange(individualAxiom_5);
			}
			
			/**
			 * Assign other inferred weather states too
			 */
			for(int i=0;i<this.objectWeatherState.size();i++){
				JSONObject x=new JSONObject();
			    String individualWeatherState=this.objectWeatherState.get(i); 
			    OWLClass otherWeatherStateClass = owlDataFactoryObject.getOWLClass(IRI.create(NS + individualWeatherState));
				OWLClassAssertionAxiom owlClassAssertionAxiom3 = owlDataFactoryObject.getOWLClassAssertionAxiom(otherWeatherStateClass, WeatherState_Current);
				owlOntologyManagerObject.addAxiom(owlOntologyObject, owlClassAssertionAxiom3);
			    x.put("weatherState", individualWeatherState);
			    x.put("weatherClothes", null);
			    this.objClothes.put(individualWeatherState, null);
				
			}
			
			/**
			 * 1. Get WeatherClothes class and create an individual WeatherClothes_Current
			 * 2. Add it to the Ontology
			 * 3. Assign hasClothing object property to the WeatherState_Current
			 */
			OWLClass weatherClothesClass = owlDataFactoryObject.getOWLClass(IRI.create(NS + "WeatherClothes"));
			OWLNamedIndividual WeatherClothes_Current = owlDataFactoryObject.getOWLNamedIndividual(IRI.create(NS+ "WeatherClothes_Current"));
			OWLClassAssertionAxiom owlClassAssertionAxiom3 = owlDataFactoryObject.getOWLClassAssertionAxiom(weatherClothesClass, WeatherClothes_Current);
			owlOntologyManagerObject.addAxiom(owlOntologyObject, owlClassAssertionAxiom3);

			OWLObjectProperty objectProperty3 = owlDataFactoryObject.getOWLObjectProperty(IRI.create(NS+ "hasClothing"));
			OWLObjectPropertyAssertionAxiom owlObjectPropertyAssertionAxiom3 = owlDataFactoryObject.getOWLObjectPropertyAssertionAxiom(objectProperty3, WeatherState_Current, WeatherClothes_Current);
			AddAxiom individualAxiom_5 = new AddAxiom(owlOntologyObject, owlObjectPropertyAssertionAxiom3);
			owlOntologyManagerObject.applyChange(individualAxiom_5);


			
			//Output to Ontology
			this.writeInferredOnto(owlOntologyManagerObject,owlOntologyObject);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Method to output new Ontology
	 * @OWLOntologyManager owlOntologyManagerObject
	 * OWLOntology owlOntologyObject
	 */
	public void writeInferredOnto(OWLOntologyManager owlOntologyManagerObject,OWLOntology owlOntologyObject){
		try{
			
			//Output the new Ontology
			owlOntologyManagerObject.saveOntology(owlOntologyObject,new RDFXMLOntologyFormat(),IRI.create(newOntfile));
			System.out.println("New Ontology created....");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Method to determine the kind of clothes
	 */
	public void determineWeatherClothes(){
		try{
			
			InputStream in = new FileInputStream(newOntfile);
			Model model = ModelFactory.createMemModelMaker().createDefaultModel();
			model.read(in, null);
			in.close();

			String queryString = 
					"PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
						"PREFIX owl: <http://www.w3.org/2002/07/owl#>"+
						"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
						"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
						"PREFIX weather: <https://www.auto.tuwien.ac.at/downloads/thinkhome/ontology/WeatherOntology.owl#>"+
						"select distinct ?ClothingClass ?SuitableFor ?Clothes ?WeatherObject where {"+
						"?ClothingClass rdfs:subClassOf weather:WeatherClothes ."+
						"?ClothingClass (rdfs:subClassOf|owl:equivalentClass)/owl:someValuesFrom ?SuitableFor;"+
						    "(rdfs:subClassOf|owl:equivalentClass)/owl:someValuesFrom/owl:oneOf/rdf:rest*/rdf:first ?Clothes."+
						"?WeatherObject a ?SuitableFor."+
						"}";


			com.hp.hpl.jena.query.Query q = QueryFactory.create(queryString);
			QueryExecution qe = QueryExecutionFactory.create(q, model);
			ResultSet results = qe.execSelect();
			ResultSet resultsX;
			
			while(results.hasNext()){
				
				QuerySolution rs=results.next();
				
				//Retrieve the WeatherState and match it in JSON
				String retrievedWeatherClothingState=rs.get("SuitableFor").toString().split("#")[1];
				String retrievedWeatherClothing=rs.get("Clothes").toString();
				ArrayList<String> existingClothes=this.objClothes.get(retrievedWeatherClothingState);
				
				if((existingClothes!=null) && (!existingClothes.contains(retrievedWeatherClothing))){
					existingClothes.add(retrievedWeatherClothing);
				}else{
					 existingClothes=new ArrayList<String>();
					existingClothes.add(retrievedWeatherClothing);
				}
				this.objClothes.put(retrievedWeatherClothingState, existingClothes);
				
			}
			qe.close();

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
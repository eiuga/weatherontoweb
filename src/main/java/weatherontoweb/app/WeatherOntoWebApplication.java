package weatherontoweb.app;


import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import weatherontoweb.rest.WeatherOntoWebService;
 
public class WeatherOntoWebApplication extends Application {
	private Set<Object> singletons = new HashSet<Object>();
 
	public WeatherOntoWebApplication() {
		singletons.add(new WeatherOntoWebService());
	}
 
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}